# rapport.irve

Package de création d'un rapport départemental sur les infrastructures de recharges des véhicules électriques d'une région.
Pour produire le rapport : installez le package, chargez-le et lancez-la fonction `compiler_rapport()`.


```
devtools::install_gitlab("dreal-datalab/rapport.irve")
library(rapport.irve)
compiler_rapport(dir = "irve", region = "52", miseajour = TRUE)

```

Un document docx comprennant une carte des points de recharge, un tableau d'indicateurs départementaux et la méthodologie sera produit dans le répertoire "irve". 